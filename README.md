# Github Expo

## Overview

Github Expo is a Flutter application designed to fetch repositories from GitHub API using "Flutter" as a query keyword. It provides features such as offline browsing, pagination, sorting, and repository details viewing to enhance user experience.

## Features

1. **Fetch Repository List**: Fetches repositories from GitHub API using "Flutter" as the query keyword.
2. **Local Database Storage**: Stores fetched data locally to enable offline browsing.
3. **Pagination**: Fetches 10 new items upon scrolling for seamless browsing experience.
4. **Data Refresh**: Automatically refreshes data from the API no more frequently than once every 30 minutes.
5. **Sortable List**: Allows sorting of the repository list by last updated date-time or star count.
6. **Persistent Sorting Option**: The selected sorting option persists across app sessions.
7. **Repository Details Page**: Navigates to a detailed view of a repository upon clicking on an item from the list.
8. **Detailed Repository Information**: Displays repository owner's name, photo, description, last update date-time, and other relevant fields.
9. **Offline Browsing Support**: Once loaded, repository list and details data are saved for offline browsing.

## Usage

1. Clone the repository to your local machine.
2. Run `flutter pub get` to install dependencies.
3. Run `dart run build_runner build --delete-conflicting-outputs` to generate required files.
4. Build and run the application on your preferred device or emulator.

## Screenshots

<style>
  td {
    text-align: center;
  }
</style>

<table align="center">
  <tr>
    <th>Light Theme</th>
    <th>Dark Theme</th>
  </tr>
  <tr>
    <td><img src="screenshots/splash_screen_light.png" alt="splash_light"style="height:400px;"></td>
    <td><img src="screenshots/splash_screen_dark.png" alt="splash_dark"style="height:400px;"></td>
  </tr>
  <tr>
    <td>Splash Screen</td>
    <td>Splash Screen</td>
  </tr>

  <tr>
    <td><img src="screenshots/home_screen_light.png" alt="home_light" style="height:400px;"></td>
    <td><img src="screenshots/home_screen_dark.png" alt="home_dark" style="height:400px;"></td>
  </tr>
  <tr>
    <td>Home Screen</td>
    <td>Home Screen</td>
  </tr>

  <tr>
    <td><img src="screenshots/repo_details_light.png" alt="details_light" style="height:400px;"></td>
    <td><img src="screenshots/repo_details_dark.png" alt="details_dark" style="height:400px;"></td>
  </tr>
  <tr>
    <td>Repository Details Screen</td>
    <td>Repository Details Screen</td>
  </tr>

  <tr>
    <td><img src="screenshots/sort_menu_popup_light.png" alt="sort_light" style="height:400px;"></td>
    <td><img src="screenshots/sort_menu_popup_dark.png" alt="sort_dark" style="height:400px;"></td>
  </tr>
  <tr>
    <td>Sort Menu Popup Screen</td>
    <td>Sort Menu Popup Screen</td>
  </tr>

  <tr>
    <td><img src="screenshots/no_data_light.png" alt="no_data_light" style="height:400px;"></td>
    <td><img src="screenshots/no_data_dark.png" alt="no_data_dark" style="height:400px;"></td>
  </tr>
  <tr>
    <td>Empty Data List Screen</td>
    <td>Empty Data List Screen</td>
  </tr>

  <tr>
    <td><img src="screenshots/no_internet_light.png" alt="no_data_light" style="height:400px;"></td>
    <td><img src="screenshots/no_internet_dark.png" alt="no_data_dark" style="height:400px;"></td>
  </tr>
  <tr>
    <td>No Internet Screen</td>
    <td>No Internet Screen</td>
  </tr>

</table>


## Architecture

The architecture of Github Expo follows a structured approach, adhering to best practices for maintainability and scalability. Key components include:

- **Data Layer**: Responsible for fetching data from the GitHub API, managing local database operations, and handling data caching for offline use.
- **UI Layer**: Consists of UI components such as screens, widgets, and controllers, facilitating user interaction and displaying fetched data in a user-friendly manner.
- **State Management**: Utilizes the GetX library for state management, leveraging its reactive capabilities to manage application state and ensure responsiveness.
- **Dependency Injection**: Leverage the power of GetX dependency injection.

## Future Enhancements

To further improve Github Expo, consider the following enhancements:

- Implementing search functionality to allow users to search for specific repositories by adding a search widget.
- Adding support for user authentication, enabling users to interact with their GitHub accounts and perform actions like starring repositories.
- Enhancing UI/UX for a more intuitive and visually appealing experience.
- Implementing unit tests and integration tests to ensure code quality and reliability.
- Optimizing network requests and caching mechanisms for improved performance.
- Supporting additional features such as bookmarking repositories or sharing them with others.
- Adding retry mechanism for API call failure.

## Contributions

Contributions to Github Expo are welcome! If you have any suggestions, bug fixes, or feature requests, feel free to open an issue or submit a pull request on the GitHub repository.

## License

Github Expo is licensed under the MIT License. See the [LICENSE](/LICENSE) file for more details.