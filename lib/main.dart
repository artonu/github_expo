import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:github_expo/app/app_controller.dart';
import 'package:github_expo/app/app_routes.dart';
import 'package:github_expo/app/themes.dart';

import 'app/strings.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  Get.put(AppController(), permanent: true);
  runApp(
    GetMaterialApp(
      initialRoute: AppRoute.launcher,
      getPages: AppRoute.pages,
      theme: lightTheme,
      darkTheme: darkTheme,
      themeMode: ThemeMode.system,
      translations: Strings(),
      locale: const Locale('en', 'US'),
      fallbackLocale: const Locale('en', 'US'),
      debugShowCheckedModeBanner: false,
    ),
  );
}
