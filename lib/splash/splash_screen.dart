import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:github_expo/app/app_routes.dart';

import '../gen/assets.gen.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    WidgetsFlutterBinding.ensureInitialized().addPostFrameCallback(
      (timeStamp) {
        Future.delayed(const Duration(seconds: 2)).then(
          (value) => Get.offNamed(AppRoute.home),
        );
      },
    );
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: Center(
        child: Image.asset(
          Assets.images.icGithub.path,
          height: 100,
          width: 100,
        ),
      ),
    );
  }
}
