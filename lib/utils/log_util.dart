import 'package:flutter/foundation.dart';
import 'package:logger/logger.dart';

class Log {
  static final Logger _logger = Logger();

  Log._internal();

  static void d(String msg) {
    if (!kReleaseMode) {
      _logger.d(msg);
    }
  }

  static void e(String msg) {
    if (!kReleaseMode) {
      _logger.e(msg);
    }
  }
}
