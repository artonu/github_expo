import 'package:intl/intl.dart';

import 'log_util.dart';

/// Formats a date-time string from one format to another.

/// This function takes a date-time string `dateTime` in the format specified by `inputFormat`
/// and converts it to a new format specified by `outputFormat`. Both `inputFormat` and
/// `outputFormat` should follow the syntax of [DateFormat][DateFormat] from the `intl` package.

/// Returns the formatted date-time string in the `outputFormat`.

/// Throws a [FormatException] if the `dateTime` string cannot be parsed according to the `inputFormat`.

String convertDateTimeStringFormat(
    String dateTime, String inputFormat, String outputFormat) {
  Log.d("Input dateTime $dateTime");
  final parsedDateTime = DateFormat(inputFormat).parse(dateTime);
  return DateFormat(outputFormat).format(parsedDateTime);
}

/// Converts milliseconds since epoch to a formatted date-time string.

/// This function takes a number of milliseconds since the Unix epoch (`milliseconds`)
/// and converts it to a formatted date-time string using the specified `outputFormat`.
/// The `outputFormat` should follow the syntax of [DateFormat][DateFormat] from the `intl` package.

/// Returns the formatted date-time string.

String millsToDateTimeString(int milliseconds, String outputFormat) {
  Log.d("Input millis $milliseconds");
  final dateTime = DateTime.fromMillisecondsSinceEpoch(milliseconds);
  return DateFormat(outputFormat).format(dateTime);
}

/// Converts a date and time string to milliseconds since epoch.

/// This function takes a string representation of a date and time and an input format string,
/// and returns the number of milliseconds since the Unix epoch (January 1, 1970, 00:00:00 UTC).

/// Parameters:
/// * `dateTime` (String): The date and time string to convert. The format must be compatible with the specified `inputFormat`.
/// * `inputFormat` (String): The format string that defines the expected format of the `dateTime` string.
///   This string should follow the syntax of [DateFormat][dart:core].

/// Returns:
/// * `int`: The number of milliseconds since the Unix epoch, representing the date and time specified in the `dateTime` string.

/// Throws:
/// * `FormatException`: If the `dateTime` string cannot be parsed according to the specified `inputFormat`.

int convertTimeStringToMillis(String dateTime, String inputFormat) {
  final parsedDateTime = DateFormat(inputFormat).parse(dateTime);
  return parsedDateTime.millisecondsSinceEpoch;
}
