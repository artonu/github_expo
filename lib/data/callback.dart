final class Callback<T> {
  final void Function(T data) onSuccess;

  final void Function(int code, String message) onError;

  Callback({required this.onSuccess, required this.onError});
}
