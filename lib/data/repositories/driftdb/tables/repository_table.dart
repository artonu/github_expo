import 'package:drift/drift.dart';

class Repositories extends Table {
  IntColumn get id => integer()();

  TextColumn get name => text()();

  TextColumn get fullName => text()();

  BoolColumn get private => boolean()();

  TextColumn get htmlUrl => text()();

  TextColumn get description => text().nullable()();

  DateTimeColumn get createdAt => dateTime()();

  DateTimeColumn get updatedAt => dateTime()();

  DateTimeColumn get pushedAt => dateTime()();

  IntColumn get size => integer()();

  IntColumn get stargazersCount => integer()();

  IntColumn get watchersCount => integer()();

  IntColumn get openIssuesCount => integer()();

  IntColumn get forksCount => integer()();

  TextColumn get visibility => text()();

  TextColumn get ownerLogin => text()();

  TextColumn get ownerAvatarUrl => text()();

  @override
  Set<Column> get primaryKey => {id};
}
