import 'package:drift/drift.dart';

import '../database.dart';

Future<int> createOrUpdateRepository(
    AppDatabase db, RepositoriesCompanion entry) {
  return db.into(db.repositories).insert(entry, mode: InsertMode.replace);
}

Stream<List<Repository>> getAllRepositories(AppDatabase db) {
  return db.select(db.repositories).watch();
}

Future<int> getDbRepositoryCounts(AppDatabase db) async {
  var repos = await db.select(db.repositories).get();
  return repos.length;
}

Future<int> deleteRepository(AppDatabase db, int id) {
  return (db.delete(db.repositories)..where((tbl) => tbl.id.equals(id))).go();
}

Future<int> deleteAllRepository(AppDatabase db) {
  return (db.delete(db.repositories)).go();
}

Future<List<Repository>> getPaginatedRepositories(
    AppDatabase db, String sort, int offset,
    {int pageSize = 10}) {
  return (db.select(db.repositories)
        ..orderBy([
          (t) => OrderingTerm(
              mode: OrderingMode.desc,
              expression: sort == "stars" ? t.stargazersCount : t.updatedAt)
        ])
        ..limit(pageSize, offset: offset))
      .get();
}
