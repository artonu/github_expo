import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:github_expo/data/repositories/driftdb/helpers/repository_table_helper.dart';
import 'package:github_expo/data/repositories/github/responses/github_repo_model.dart';
import 'package:github_expo/data/repositories/github/responses/search_result.dart';
import 'package:github_expo/data/repositories/utils/repo_util.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

import '../../utils/log_util.dart';
import '../callback.dart';
import 'driftdb/database.dart';
import 'github/github_repository.dart';

class AppDataRepository {
  static final AppDataRepository _instance = AppDataRepository._internal();
  late final Dio dio;
  late final GithubRestRepository _githubRestRepository;
  late AppDatabase _driftDatabase;

  factory AppDataRepository() {
    return _instance;
  }

  AppDataRepository._internal() {
    dio = Dio();
    if (kDebugMode) {
      dio.interceptors.add(
        PrettyDioLogger(
            requestHeader: true,
            requestBody: true,
            responseBody: true,
            responseHeader: true,
            error: true,
            compact: true,
            maxWidth: 90),
      );
    }
    _githubRestRepository = GithubRestRepository(dio);
    _driftDatabase = AppDatabase();
  }

  void fetchRepositories({
    required String query,
    required String sortBy,
    required String order,
    required int perPage,
    required int page,
    required Callback<SearchResult> callback,
  }) async {
    try {
      var response = await _githubRestRepository.fetchRepository(
          query, sortBy, order, perPage, page);
      callback.onSuccess(response);
    } catch (error) {
      var errorCode = -1;
      var errorMsg = "$error";
      switch (error.runtimeType) {
        case DioException _:
          errorCode = (error as DioException).response?.statusCode ?? -1;
          errorMsg = (error).message ?? "Unknown error";
          break;
        default:
          Log.e("fetchRepositories >> ${error.runtimeType}");
          break;
      }
      callback.onError(errorCode, errorMsg);
    }
  }

  Future<int> saveRepositoryToDb(GithubRepo repo) async {
    var dbObject = flattenRepositoryResponseToDbObject(repo);
    return await createOrUpdateRepository(_driftDatabase, dbObject);
  }

  Future<void> saveAllRepositoryToDb(List<GithubRepo> list) async {
    for (GithubRepo gr in list) {
      await saveRepositoryToDb(gr);
    }
  }

  Future<int> getRepositoryCountsInDb() {
    return getDbRepositoryCounts(_driftDatabase);
  }

  Future<List<Repository>> getRepositoryFromDb(String sort, int offset) {
    return getPaginatedRepositories(_driftDatabase, sort, offset);
  }

  Future<int> deleteAllRepositoryFromDb() {
    return deleteAllRepository(_driftDatabase);
  }
}
