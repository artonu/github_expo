import 'package:github_expo/app/constants.dart';
import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart' hide Headers;

import 'responses/search_result.dart';

part 'github_repository.g.dart';

@RestApi(baseUrl: githubRestApiBaseUrl)
abstract class GithubRestRepository {
  factory GithubRestRepository(Dio dio, {String baseUrl}) =
      _GithubRestRepository;

  @GET('/search/repositories')
  @Headers(<String, dynamic>{
    'Accept': 'application/vnd.github+json',
    'X-GitHub-Api-Version': '2022-11-28'
  })
  Future<SearchResult> fetchRepository(
    @Query('q') String query,
    @Query('sort') String sort,
    @Query('order') String order,
    @Query('per_page') int perPage,
    @Query('page') int page,
  );
}
