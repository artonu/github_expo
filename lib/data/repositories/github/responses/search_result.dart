import 'package:github_expo/data/repositories/github/responses/github_repo_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'search_result.g.dart';

@JsonSerializable()
class SearchResult {
  SearchResult({
    required this.totalCount,
    required this.incompleteResults,
    required this.items,
  });

  late final int totalCount;
  late final bool incompleteResults;
  late final List<GithubRepo> items;

  SearchResult.fromJson(Map<String, dynamic> json) {
    totalCount = json['total_count'];
    incompleteResults = json['incomplete_results'];
    items =
        List.from(json['items']).map((e) => GithubRepo.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['total_count'] = totalCount;
    _data['incomplete_results'] = incompleteResults;
    _data['items'] = items.map((e) => e.toJson()).toList();
    return _data;
  }
}
