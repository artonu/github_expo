import 'package:json_annotation/json_annotation.dart';

part 'github_repo_model.g.dart';

@JsonSerializable()
class GithubRepo {
  GithubRepo({
    required this.id,
    required this.name,
    required this.fullName,
    required this.private,
    required this.owner,
    required this.htmlUrl,
    required this.description,
    required this.createdAt,
    required this.updatedAt,
    required this.pushedAt,
    required this.size,
    required this.stargazersCount,
    required this.watchersCount,
    required this.visibility,
    required this.openIssuesCount,
    required this.forksCount,
  });

  late final int id;
  late final String name;
  late final String fullName;
  late final bool private;
  late final Owner owner;
  late final String htmlUrl;
  late final String? description;
  late final String createdAt;
  late final String updatedAt;
  late final String pushedAt;
  late final int size;
  late final int stargazersCount;
  late final int watchersCount;
  late final String visibility;
  late final int openIssuesCount;
  late final int forksCount;

  GithubRepo.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    fullName = json['full_name'];
    private = json['private'];
    owner = Owner.fromJson(json['owner']);
    htmlUrl = json['html_url'];
    description = json['description'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    pushedAt = json['pushed_at'];
    size = json['size'];
    stargazersCount = json['stargazers_count'];
    watchersCount = json['watchers_count'];
    visibility = json['visibility'];
    openIssuesCount = json['open_issues_count'];
    forksCount = json['forks_count'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['name'] = name;
    _data['full_name'] = fullName;
    _data['private'] = private;
    _data['owner'] = owner.toJson();
    _data['html_url'] = htmlUrl;
    _data['description'] = description;
    _data['created_at'] = createdAt;
    _data['updated_at'] = updatedAt;
    _data['pushed_at'] = pushedAt;
    _data['size'] = size;
    _data['stargazers_count'] = stargazersCount;
    _data['watchers_count'] = watchersCount;
    _data['visibility'] = visibility;
    _data['open_issues_count'] = openIssuesCount;
    _data['forks_count'] = forksCount;
    return _data;
  }
}

class Owner {
  Owner({
    required this.login,
    required this.avatarUrl,
  });

  late final String login;
  late final String avatarUrl;

  Owner.fromJson(Map<String, dynamic> json) {
    login = json['login'];
    avatarUrl = json['avatar_url'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['login'] = login;
    _data['avatar_url'] = avatarUrl;
    return _data;
  }
}
