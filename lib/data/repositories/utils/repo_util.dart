import 'package:drift/drift.dart' as drift;
import 'package:github_expo/app/constants.dart';
import 'package:github_expo/data/repositories/driftdb/database.dart';
import 'package:github_expo/data/repositories/github/responses/github_repo_model.dart';

import '../../../utils/functions.dart';

RepositoriesCompanion flattenRepositoryResponseToDbObject(GithubRepo repo) {
  return RepositoriesCompanion(
    id: drift.Value(repo.id),
    name: drift.Value(repo.name),
    fullName: drift.Value(repo.fullName),
    private: drift.Value(repo.private),
    htmlUrl: drift.Value(repo.htmlUrl),
    description: drift.Value(repo.description),
    size: drift.Value(repo.size),
    stargazersCount: drift.Value(repo.stargazersCount),
    watchersCount: drift.Value(repo.watchersCount),
    openIssuesCount: drift.Value(repo.openIssuesCount),
    forksCount: drift.Value(repo.forksCount),
    visibility: drift.Value(repo.visibility),
    createdAt: drift.Value(DateTime.parse(repo.createdAt)),
    updatedAt: drift.Value(DateTime.parse(repo.updatedAt)),
    pushedAt: drift.Value(DateTime.parse(repo.pushedAt)),
    ownerLogin: drift.Value(repo.owner.login),
    ownerAvatarUrl: drift.Value(repo.owner.avatarUrl),
  );
}

GithubRepo constructRepoModelFromDbObject(Repository repo) {
  return GithubRepo(
    id: repo.id,
    name: repo.name,
    fullName: repo.fullName,
    private: repo.private,
    owner: Owner(
      login: repo.ownerLogin,
      avatarUrl: repo.ownerAvatarUrl,
    ),
    htmlUrl: repo.htmlUrl,
    description: repo.description,
    createdAt: millsToDateTimeString(
        repo.createdAt.millisecondsSinceEpoch, apiResponseDateFormat),
    updatedAt: millsToDateTimeString(
        repo.updatedAt.millisecondsSinceEpoch, apiResponseDateFormat),
    pushedAt: millsToDateTimeString(
        repo.pushedAt.millisecondsSinceEpoch, apiResponseDateFormat),
    size: repo.size,
    stargazersCount: repo.stargazersCount,
    watchersCount: repo.watchersCount,
    openIssuesCount: repo.openIssuesCount,
    forksCount: repo.forksCount,
    visibility: repo.visibility,
  );
}
