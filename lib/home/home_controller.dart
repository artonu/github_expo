import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:github_expo/data/callback.dart';
import 'package:github_expo/data/repositories/app_data_repository.dart';
import 'package:github_expo/data/repositories/github/responses/github_repo_model.dart';
import 'package:github_expo/data/repositories/github/responses/search_result.dart';
import 'package:github_expo/data/repositories/utils/repo_util.dart';
import 'package:github_expo/home/sort_order.dart';
import 'package:github_expo/utils/functions.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../app/app_controller.dart';
import '../app/constants.dart';
import '../app/strings.dart';
import '../utils/log_util.dart';

class HomeController extends GetxController {
  final AppController _appController = Get.find();
  late final SharedPreferences _sharedPreference;
  static const _keyApiSyncTime = "last_synced";
  static const _keyCurrentPage = "current_page";
  static const _sessionKeySortOrder = "sort_order";
  var selectedSortType = SortOrder.lastUpdateTime.obs;
  static const _fetchInterval = 30 * 60 * 1000;
  final _scrollController = ScrollController();
  final _repos = <GithubRepo>[];
  int _currentPage = 1;
  var isLoading = false.obs;
  int lastSync = 0;
  int totalItem = -1;
  int _totalDbItemCount = -1;

  get repos => _repos;

  get itemCount => _repos.length;

  get hasMoreItems =>
      _totalDbItemCount > _repos.length ||
      totalItem == -1 ||
      _totalDbItemCount < totalItem;

  get scrollController => _scrollController;

  @override
  Future<void> onInit() async {
    isLoading.value = true;
    _sharedPreference = await SharedPreferences.getInstance();
    _loadSortOrder();
    _loadSyncTimeAndPageIndex();
    loadData(false);
    super.onInit();
  }

  GithubRepo getRepository(int index) {
    return _repos[index];
  }

  Future<void> _loadRepositoriesFromApi(bool isRefreshed) async {
    var completer = Completer<void>();
    isLoading.value = true;
    final Callback<SearchResult> callback = Callback(
      onSuccess: (data) async {
        Log.d("received ${data.items.length} repo");
        // Get current scroll position before insertion
        totalItem = data.totalCount;
        if (data.items.isNotEmpty) {
          await AppDataRepository().saveAllRepositoryToDb(data.items);
          _totalDbItemCount =
              await AppDataRepository().getRepositoryCountsInDb();
          _currentPage++;
          await _saveCurrentPageIndex(_currentPage);
        }
        completer.complete();
      },
      onError: (code, msg) {
        Log.e("Error code: $code msg: $msg");
        completer.complete();
      },
    );
    var nowSinceEpoch = DateTime.now().millisecondsSinceEpoch;
    if (isRefreshed || (nowSinceEpoch - lastSync).abs() >= _fetchInterval) {
      _currentPage = 1;
      _repos.clear();
      update();
      isLoading.value = true; // trigger again after data clear
      lastSync = nowSinceEpoch;
      await _sharedPreference.setInt(_keyApiSyncTime, lastSync);
      _saveCurrentPageIndex(_currentPage);
    }

    String query =
        "flutter pushed:<=${millsToDateTimeString(lastSync, apiResponseDateFormat)}";
    AppDataRepository().fetchRepositories(
        query: query,
        sortBy: selectedSortType.value.value,
        order: "desc",
        perPage: 10,
        page: _currentPage,
        callback: callback);

    return completer.future;
  }

  Future<void> _saveCurrentPageIndex(int index) async {
    await _sharedPreference.setInt(_keyCurrentPage, index);
  }

  Future<void> onSortOrderChanged(SortOrder order) async {
    selectedSortType.value = order;
    await _sharedPreference.setString(_sessionKeySortOrder, order.value);
    _currentPage = 1;
    await _saveCurrentPageIndex(_currentPage);
    await AppDataRepository().deleteAllRepositoryFromDb();
    _repos.clear();
    update();
  }

  void _loadSortOrder() {
    var sortOrder = _sharedPreference.getString(_sessionKeySortOrder);
    sortOrder == null
        ? selectedSortType.value = SortOrder.lastUpdateTime
        : selectedSortType.value = SortOrder.fromString(sortOrder);
  }

  Future<void> _loadSyncTimeAndPageIndex() async {
    lastSync = _sharedPreference.getInt(_keyApiSyncTime) ?? 0;
    _currentPage = _sharedPreference.getInt(_keyCurrentPage) ?? 1;
    var nowSinceEpoch = DateTime.now().millisecondsSinceEpoch;
    Log.d("Current unix time millis $nowSinceEpoch last sync $lastSync");
    if (lastSync == 0) {
      Log.d("Saved last sync time is null");
      lastSync = nowSinceEpoch;
      await _sharedPreference.setInt(_keyApiSyncTime, lastSync);
      _saveCurrentPageIndex(1);
      _currentPage = 1;
    } else if ((nowSinceEpoch - lastSync).abs() >= _fetchInterval) {
      Log.d("Saved last sync time is over 30 mins ago");
      lastSync = nowSinceEpoch;
      await _sharedPreference.setInt(_keyApiSyncTime, lastSync);
      _saveCurrentPageIndex(1);
      _currentPage = 1;
    }
  }

  void sortByStarCount() {
    _repos.sort((a, b) => a.stargazersCount.compareTo(b.stargazersCount));
    update();
  }

  void sortByUpdatedAt() {
    _repos.sort((a, b) => a.updatedAt.compareTo(b.updatedAt));
    update();
  }

  void scrollToTop() {
    _scrollController.jumpTo(0);
  }

  Future<void> loadData(bool isRefreshed) async {
    isLoading.value = true;
    _totalDbItemCount = await AppDataRepository().getRepositoryCountsInDb();
    Log.d("loadData >> DB total item count: $_totalDbItemCount");

    if (isRefreshed) _repos.clear();
    int page = (_repos.length / 10).floor();
    var remainder = _repos.length.remainder(10);
    int offset = page * 10;
    if (remainder > 0 && remainder < 10) {
      offset += remainder;
    }

    Log.d(
        "loadData >> isRefresh : $isRefreshed DB page: $page Offset: $offset");
    var dbRepoData = await AppDataRepository()
        .getRepositoryFromDb(selectedSortType.value.value, offset);
    Log.d("loadData >> DB fetched : ${dbRepoData.length} item");

    if (dbRepoData.isEmpty) {
      if (_appController.hasInternet.value) {
        Log.d("loadData >> no data from DB trying to get from internet");
        await _loadRepositoriesFromApi(isRefreshed);
        dbRepoData = await AppDataRepository()
            .getRepositoryFromDb(selectedSortType.value.value, offset);
      } else {
        Log.d("loadData >> no data from DB internet not available");
        isLoading.value = false;
        _showNoInternetSnackBar();
        return;
      }
    }
    Log.d("loadData >> got ${dbRepoData.length} items updating list");
    if (dbRepoData.isNotEmpty) {
      final oldPosition = _scrollController.position.pixels;
      _repos.addAll(
        dbRepoData.map((item) => constructRepoModelFromDbObject(item)),
      );
      isLoading.value = false;
      update();
      _scrollController.jumpTo(oldPosition);
    }
    isLoading.value = false;
  }

  void _showNoInternetSnackBar() {
    Get.snackbar(
      Strings.titleNoInternet.tr,
      Strings.msgNoInternet.tr,
      backgroundColor: Colors.red,
      colorText: Colors.white,
    );
  }
}
