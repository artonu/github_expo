enum SortOrder {
  lastUpdateTime("updated"),
  starCount("stars");

  const SortOrder(this.value);

  final String value;

  factory SortOrder.fromString(String stringValue) {
    return values.firstWhere((e) => e.value == stringValue,
        orElse: () => throw ArgumentError(
            'Invalid SortOrder string value: $stringValue'));
  }
}
