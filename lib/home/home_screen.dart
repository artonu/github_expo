import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:github_expo/home/home_controller.dart';
import 'package:github_expo/home/repo_details_bottomsheet.dart';
import 'package:github_expo/home/sort_order.dart';
import 'package:github_expo/widgets/item_repo_widget.dart';
import 'package:loadmore_listview/loadmore_listview.dart';

import '../app/colors.dart';
import '../app/strings.dart';
import '../utils/log_util.dart';

class HomeScreen extends StatelessWidget {
  final HomeController _controller = Get.put(HomeController());

  HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(Strings.keyAppBarTitle.tr),
        centerTitle: true,
        actions: [
          Obx(
            () => PopupMenuButton<SortOrder>(
              initialValue: _controller.selectedSortType.value,
              position: PopupMenuPosition.under,
              icon: const Icon(
                Icons.sort_rounded,
              ),
              onSelected: (SortOrder item) async {
                var oldSort = _controller.selectedSortType.value;
                if (oldSort != item) {
                  await _controller.onSortOrderChanged(item);
                  await _controller.loadData(true);
                  _controller.scrollToTop();
                }
              },
              itemBuilder: (BuildContext context) =>
                  <PopupMenuEntry<SortOrder>>[
                PopupMenuItem<SortOrder>(
                  value: SortOrder.lastUpdateTime,
                  child: Text(Strings.keySortByDate.tr),
                ),
                PopupMenuItem<SortOrder>(
                  value: SortOrder.starCount,
                  child: Text(Strings.keySortByStarCount.tr),
                ),
              ],
            ),
          ),
        ],
      ),
      body: Stack(
        children: [
          Obx(
            () => Visibility(
              visible:
                  _controller.isLoading.value && _controller.itemCount == 0,
              child: const Center(
                child: CircularProgressIndicator(),
              ),
            ),
          ),
          Obx(
            () => Visibility(
              visible:
                  !_controller.isLoading.value && _controller.itemCount == 0,
              child: const Center(
                child: Text(
                  "No data found",
                  style: TextStyle(
                    color: Colors.blueAccent,
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ),
          GetBuilder<HomeController>(
            builder: (HomeController controller) {
              return LoadMoreListView.builder(
                //is there more data to load
                hasMoreItem: controller.hasMoreItems,
                //Trigger the bottom loadMore callback
                onLoadMore: () async {
                  //wait for your api to fetch more items
                  Log.d("Loading more repos");
                  await controller.loadData(false);
                },
                //pull down refresh callback
                onRefresh: () async {
                  //wait for your api to update the list
                  Log.d("Refreshing repos");
                  await controller.loadData(true);
                },
                //you can set your loadMore Animation
                loadMoreWidget: Container(
                  margin: const EdgeInsets.all(20.0),
                  alignment: Alignment.center,
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(context.isDarkMode
                        ? repoCardItemBgDarkColor
                        : repoCardItemBgLightColor),
                  ),
                ),
                //ListView
                itemCount: controller.itemCount,
                controller: _controller.scrollController,
                itemBuilder: (context, index) {
                  var repository = controller.getRepository(index);
                  return InkWell(
                    borderRadius: BorderRadius.circular(10),
                    onTap: () {
                      Get.bottomSheet(
                        RepoDetailsBottomSheet(repository),
                      );
                    },
                    child: ItemRepoWidget(repository),
                  );
                },
              );
            },
          ),
        ],
      ),
    );
  }
}
