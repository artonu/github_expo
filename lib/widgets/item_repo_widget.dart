import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:github_expo/app/constants.dart';

import '../app/colors.dart';
import '../data/repositories/github/responses/github_repo_model.dart';
import '../gen/assets.gen.dart';
import '../utils/functions.dart';

class ItemRepoWidget extends StatelessWidget {
  final GithubRepo repository;

  const ItemRepoWidget(this.repository, {super.key});

  @override
  Widget build(BuildContext context) {
    return Card(
      color: context.isDarkMode
          ? repoCardItemBgDarkColor
          : repoCardItemBgLightColor,
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              repository.name,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(
                  child: Text(
                    "Updated At: ${convertDateTimeStringFormat(repository.updatedAt, apiResponseDateFormat, appDateFormat)}",
                    style: const TextStyle(
                      fontSize: 16,
                    ),
                  ),
                ),
                Image.asset(
                  Assets.images.icStar.path,
                  height: 20,
                  width: 20,
                ),
                const SizedBox(
                  width: 10,
                ),
                Text(
                  "${repository.stargazersCount}",
                  style: const TextStyle(
                    fontSize: 16,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
