import 'package:get/get.dart';

class Strings extends Translations {
  static const String keyAppBarTitle = 'app_bar_title';
  static const String keySortByDate = 'sort_by_date';
  static const String keySortByStarCount = 'sort_by_star_count';
  static const String titleNoInternet = 'title_no_internet';
  static const String msgNoInternet = 'msg_no_internet';
  static const String repositoryInfo = 'repository_info';
  static const String ownerInfo = 'owner_info';

  @override
  Map<String, Map<String, String>> get keys => {
        'en_US': {
          keyAppBarTitle: 'Github Expo',
          keySortByDate: 'By last updated',
          keySortByStarCount: 'By star count',
          titleNoInternet: 'No internet',
          msgNoInternet: 'Please connect to the internet',
          repositoryInfo: 'Repository info:',
          ownerInfo: 'Owner info:',
        },
      };
}
