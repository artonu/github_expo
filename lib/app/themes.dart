// light_theme.dart
import 'package:flutter/material.dart';
import 'package:github_expo/gen/fonts.gen.dart';

ThemeData lightTheme = ThemeData(
  useMaterial3: true,
  colorScheme: ColorScheme.fromSeed(
    seedColor: Colors.blue,
    brightness: Brightness.light,
    primary: Colors.blue,
  ),
  scaffoldBackgroundColor: Colors.white,
  fontFamily: FontFamily.poppins,
);

// dark_theme.dart
ThemeData darkTheme = ThemeData(
  useMaterial3: true,
  colorScheme: ColorScheme.fromSeed(
    seedColor: Colors.black54,
    brightness: Brightness.dark,
    primary: Colors.deepPurple,
  ),
  scaffoldBackgroundColor: Colors.black,
  fontFamily: FontFamily.poppins,
);
