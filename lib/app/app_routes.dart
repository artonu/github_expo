import 'package:get/get.dart';

import '../home/home_screen.dart';
import '../splash/splash_screen.dart';

final class AppRoute {
  AppRoute._internal();

  static const String launcher = "/";
  static const String home = "/home";

  static var pages = [
    GetPage(
      name: AppRoute.launcher,
      page: () => const SplashScreen(),
    ),
    GetPage(
      name: AppRoute.home,
      page: () => HomeScreen(),
    ),
  ];
}
