import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:get/get.dart';

class AppController extends GetxController {
  late final StreamSubscription<List<ConnectivityResult>> _connSubscription;
  var hasInternet = true.obs;

  @override
  void onInit() {
    super.onInit();
    _connSubscription = Connectivity()
        .onConnectivityChanged
        .listen((List<ConnectivityResult> result) {
      // Received changes in available connectivity types!
      if (result.isNotEmpty) {
        var connectivityResult = result[0];
        if (connectivityResult == ConnectivityResult.mobile ||
            connectivityResult == ConnectivityResult.wifi) {
          hasInternet.value = true;
        } else if (connectivityResult == ConnectivityResult.none) {
          hasInternet.value = false;
        }
      }
    });
  }

  @override
  void dispose() {
    _connSubscription.cancel();
    super.dispose();
  }
}
