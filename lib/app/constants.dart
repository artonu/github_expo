const String githubRestApiBaseUrl = "https://api.github.com/";
const String apiResponseDateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'";
const String appDateFormat = "MM-dd-yyyy hh:ss";
